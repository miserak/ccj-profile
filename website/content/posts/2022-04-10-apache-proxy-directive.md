---
title:  "Apache Directive to Proxy"
date:   2022-04-10 05:00:00-0400
author: "Charles Jeff Jones"
draft: false
---

On occasion, it is appropriate to direct a (sub-)directory to an
internal service:

```
<Location "/">
  ProxyPass http://10.0.0.1:8080/another-website/
  ProxyPassReverse http://10.0.0.1:8080/another-website/
</Location>
```
