---
layout: page
title: "Résumé/CV"
menu: "main"
date: 2022-04-10T05:00:00-04:00
draft: false
---

[Curriculum Vitae (PDF)](/pdf/jones_charles_jeff-cv.pdf)

## Professional Experience

### Technology Manager

|||
| --- | --- | --- |
| *Hurricane Wind Power* | Salem, VA | **May 2019 -- Feburary 2021** |

#### Company Description

Hurricane Wind Power is an alternative energy company focused on solar
and wind power for residential locations both on and off the power
grid.

#### Role Responsibilities

Responsible for analyzing detailed business requirements; providing
mapping between requirements and software solutions; gathering and
documenting business requirements and business processes; researching
and compiling COTS solutions; reviewed and evaluated technical design
with quality assurance techniques.

#### Achievements

+ Developed an API to communicate with hardware components.  This was
  utilized to store data in a company database and to provide a Web UI
  for customers.

### Technology Tutor

|||
| --- | --- | --- |
| *Virginia Western Community College* | Roanoke, VA | **August 2018 -- May 2019** |

#### Company Description

Virginia Western Community College (VWCC) is a public community
college in Roanoke, Virginia. It is part of the Virginia Community
College System.

#### Role Responsibilities

Responsible for student learning, growth, and advancement;
demonstrating academic competence in computer science and building a
study plan; maintaining a growth mindset toward student learning and
teaching practice; facilitating problem-solving with a curious mind
and critical thinking skills; collaborating with academic staff to
increase student performance.


## Education
### George Mason University

+ Bachelor of Science in Computer Science

### Virginia Western Community Collage

+ Associate of Science in Computer Science


## Programming Languages

|||
| ------------------------- | ----------------------------------------------- |
| Main                      | Java, Python                                    |
| Expericenced              | Bash, CSS, HTML, SQL                            |
| Used in the Past          | C, C#, C++, Golang (Go), JavaScript, Latex, PHP |
| Interests and Curiosities | Haskell, PowerShell, Scala                      |

## Databases/Datastores

|||
| ----- | ------------- |
| SQL   | MS SQL, MySQL |
| NoSQL | MongoDB       |
| Other | JSON, XML     |

## Tools and Frameworks

|||
| ------------------------- | ---------------------------------------- |
| Main                      | Apache Maven, Git, Hibernate (ORM), Vim  |
| Experienced               | Bootstrap, Eclipse (IDE), Spring Boot    |
| Used in the Past          | Gradle, Jenkins (CI/CD), SoapUI, Webpack |
| Interests and Curiosities | Hadoop, ReactJS, SonarQube               |

## Operating Systems

|||
| --------- | --------------------------------------- |
| GNU/Linux | CentOS, Debian, Fedora, Ubuntu          |
| Cisco     | IOS                                     |
