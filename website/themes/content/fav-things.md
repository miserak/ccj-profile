---
title: Favorite Things
date: 2017-09-01T05:00:00-04:00
menu: "main"
draft: false
---

## Table of Contents

- [Operating Systems](#operating-systems)
- [Studying](#studying)
- [Computer Science](#computer-science)
- [IT Operations](#it-operations)
- [Working Out](#working-out)
- [Finance](#finance)
- [Copywriting](#copywriting)
- [Liberal Arts](#liberal-arts)
- [Multimedia](#multimedia)

---

## Operating Systems

| Link | Description |
| ---- | ----------- |
| [Ubuntu](https://ubuntu.com/) | A great introductory Linux OS. |
| [KDE Neon](https://neon.kde.org/) | A great OS for visual customization. |
| [FreeBSD](https://www.freebsd.org/) | A reliable Unix. |

---

## Studying
### Practical Skills

| Link | Description |
| ---- | ----------- |
| [GNU Typist](https://www.gnu.org/software/gtypist/) | A free program to learn touch typing. |
| [How to Study in College](https://www.amazon.com/How-Study-College-Walter-Pauk/dp/1133960782) | A textbook (by Walter Pauk and Ross J.Q. Owens) with all the study skill you should have been taught in High School. |
| [Stanford University's Student Learning Programs](https://studentlearning.stanford.edu/academic-skills/tips-and-tools) | Reaching your academic potential can be challenging, but learning effective habits can make a significant difference. |
| [How to Read a Book](https://www.amazon.com/How-Read-Book-Classic-Intelligent-dp-1476790159/dp/1476790159/) | You cannot read all the books in the world. Thus, it is important to maximize what you get out of books. |
| [Getting Things Done](https://www.amazon.com/Getting-Things-Done-Stress-free-Productivity/dp/0349408947/) | A system when a simple TO-DO list no longer works. |

### Memorization

| Link | Description |
| ---- | ----------- |
| [Moonwalking with Einstein](https://www.amazon.com/Moonwalking-Einstein-Science-Remembering-Everything/dp/0143120530/) | A story about mental athletics. |
| [Your Memory: How It Works and How to Improve It](https://www.amazon.com/Your-Memory-How-Works-Improve/dp/1569246297/) | A great book to understand why we remember some things and forget others. |
| [The Memory Book](https://www.amazon.com/Memory-Book-Classic-Improving-School/dp/0345410025/) | A book full of memory techniques. |

### Grammar

| Link | Description |
| ---- | ----------- |
| [English Composition and Grammar: Complete Course](https://www.amazon.com/English-Composition-Grammar-Complete-Benchmark/dp/0153117362/) | A great resource on standard English grammar. |

### Tools

| Link | Description |
| ---- | ----------- |
| [The Unix `look` command](https://linux.die.net/man/1/look) | Display words beginning with a given string. |
| [The Unix dictionary](https://linux.die.net/man/1/dict) | A Perl client for accessing network dictionary servers. It contains to computing dictionary. |
| [LanguageTool](https://github.com/languagetool-org/languagetool) | A better alternative to Grammarly. |
| [GNU Style and Diction](https://www.gnu.org/software/diction/) |  Diction identifies wordy and commonly misused phrases. Style analyses surface characteristics of a document, including sentence length and other readability measures. |
| [Org-Mode for Emacs](https://orgmode.org/) | Org mode is for keeping notes, maintaining to-do lists, planning projects, authoring documents, computational notebooks, literate programming and more — in a fast and effective plain text system. |

### Document Generation

| Link | Description |
| ---- | ----------- |
| [Latex](https://latex-tutorial.com/) | The best typesetting software for document generation. |
| [Beamer (Latex)](https://www.overleaf.com/learn/latex/Beamer) | Beamer is a powerful and flexible LaTeX class to create great looking presentations. |
| [Graphviz (Dot)](https://graphviz.org/) | A great way to make simple graphs. |
| [TikZ (Latex package)](https://www.overleaf.com/learn/latex/TikZ_package) | TikZ is probably the most complex and powerful tool to create graphic elements in Latex. |

---

## Computer Science
### Starting Out

| Link | Description |
| ---- | ----------- |
| [How to Design Programs (HTDP)](https://htdp.org/) | A great introduction to programming.  Frankly it should be required for High School. |
| [HTDP - Istanbul Bilgi University](https://www.youtube.com/watch?v=pckMEFmQwKw&list=PLA_-EWSPTJcuy5VyJyvemxydu16kcAK0S) | A companion course to HTDP. |
| [Programmer Competency Matrix](https://sijinjoseph.netlify.app/programmer-competency-matrix/) | A good overview of skills for a programmer. |
| [Software Engineering Body of Knowledge (SWEBOK)](https://www.computer.org/education/bodies-of-knowledge/software-engineering/v3) | Covers the technical and business skills required of a professional Software Engineer. |
| [ACM's CS Curricula](https://www.acm.org/binaries/content/assets/education/cs2013_web_final.pdf) | An internationally recognized curricula published every decade. |
| [Stanford Engineering Everywhere](https://see.stanford.edu/) | Covers 80% of the ACM's CS Curricula (2013). |
| [The Missing Semester of Your CS Education](https://missing.csail.mit.edu/) | Review of required technical sophistication. |

### Coding Interviews

| Link | Description |
| ---- | ----------- |
| [LeetCode](https://leetcode.com/) | Reviews data structures and algorithms. It is a great place to practice for code interviews. |

### Exploration

| Link | Description |
| ---- | ----------- |
| [Hacker News](https://news.ycombinator.com/) | A great place for interesting discussion.  It has a "Ask HN: Who is hiring?" discussion every month. |
| [Paul Graham's Website](http://paulgraham.com/) | A great collection of essays. |
| [Operating Systems: Three Easy Pieces](https://pages.cs.wisc.edu/~remzi/OSTEP/) | A great text on Operating Systems. |

### Tools

| Link | Description |
| ---- | ----------- |
| [Artistic Style](http://astyle.sourceforge.net/) |  Artistic Style is a source code indenter, formatter, and beautifier for the C, C++, C++/CLI, Objective‑C, C#, and Java programming languages. |
| [Doxygen](https://www.doxygen.nl/) | Doxygen is the de facto standard tool for generating documentation from annotated C++ sources. |

---

## IT Operations

| Link | Description |
| ---- | ----------- |
| [Ops School](https://www.opsschool.org/) | Learn IT Operation skill sets from beginner to expert. |
| [XCP-ng](https://xcp-ng.org/) | Turnkey server virtualization. A non-Citrix fork on XenServer. |

---

## Working Out

| Link | Description |
| ---- | ----------- |
| [Coaching Olympic Style Boxing (1994)](https://www.amazon.com/Coaching-Olympic-Style-Boxing-Amateur/dp/1884125255/) | The best Boxing book for both Coaches and Fighters. |
| [Convict Conditioning by Paul "Coach" Wade](https://www.amazon.com/Convict-Conditioning-Weakness-Survival-Strength/dp/1942812159/) | An excellent training guide on body weight conditioning. |

---

## Finance

| Link | Description |
| ---- | ----------- |
| [The Richest Man in Babylon](https://www.amazon.com/gp/product/1508524351/) | A great book on the importance of savings. |
| [Get Rich Slowly (1992)](https://www.amazon.com/Get-Rich-Slowly-Building-Financial/dp/0026132117/) | A book to get an overview of the stock market. |
| [The Bogleheads' Guide to the Three-Fund Portfolio](https://www.amazon.com/gp/product/1119487331/) | A moderately conservative investment strategy and its reasoning. |

---

## Copywriting

| Link | Description |
| ---- | ----------- |
| [The Boron Letters](https://www.amazon.com/Boron-Letters-Gary-C-Halbert/dp/1484825985/) | A great book on copywriting that provided me insight into why ad-tech is in demand. |

---

## Liberal Arts

| Link | Description |
| ---- | ----------- |
| [How to Win Friend and Influence People](https://www.amazon.com/How-Win-Friends-Influence-People/dp/0671027034/) | A great book on best-in-class social skills. |
| [Class: A Guide Through the American Status System (1992)](https://www.amazon.com/gp/product/0671792253/) | A dated but great guide to help understand the habits of different socio-economic classes. |
| [Gateway to the Great Books](https://www.amazon.com/Gateway-Great-Books-10-Set/dp/B000K078GY/) | The foundation of a Liberal Arts education. |

### History

| Link | Description |
| ---- | ----------- |
| [A Collection of Unmitigated Pedantry](https://acoup.blog/) | A look at history and popular culture. |

### Art

| Link | Description |
| ---- | ----------- |
| [Myron Barnstone Studios](https://www.barnstonestudios.com/) | A great resource on drawing. |
| [Bob Ross Painting](https://www.bobross.com/) | A great resource on oil painting. |

### Education

| Link | Description |
| ---- | ----------- |
| [The Closing of the American Mind](https://www.amazon.com/Closing-American-Mind-Allan-Bloom/dp/0671479903/) | A book about the 20th century's education system. |

---

## Multimedia 

| Link | Description |
| ---- | ----------- |
| [Trakt](https://trakt.tv/) | Automatically track what you're watching. |
| [TrueNAS](https://www.truenas.com/) | A great multimedia server OS. |

---

[Top of page](#table-of-contents)
