---
title: "Links"
date: 2017-09-01T05:00:00-04:00
menu: "main"
draft: false
---

## External

1.  [Taxi Server](https://servermuffin.com:2222)
2.  [Cloud Server](http://metaldragons.info/nextcloud/index.html)
3.  [Book Server](http://servermuffin.com:2202)
4.  [R Studio](http://servermuffin.com:8787)
5.  [Shiny Server (R)](http://servermuffin.com:3838/dmiserak/)
6.  [Transmission Server](http://servermuffin.com:9091)
7.  [Media Server](http://servermuffin.com:8096)
8.  [Stock App](https://miserak.shinyapps.io/stock_app/)
9.  [Retirement App](https://miserak.shinyapps.io/retirement/)
10. [Civ RP Tech Pyramid](https://miserak.shinyapps.io/tech_pyramid/)

## Local

1.  [Taxi Server](https://10.0.0.255:4200)
2.  [Cloud Server](http://10.0.0.20)
3.  [Book Server](http://10.255.255.5:2202)
4.  [R Studio](http://10.0.0.15:8787)
5.  [Shiny Server (R)](http://10.0.0.15:3838)
6.  [Transmission Server](http://10.255.255.2:9091)
7.  [Media Server](http://10.255.255.3:8096)
8.  [Stock App](http://10.0.0.5:3838/dmiserak/stock_analysis_tool/)
9.  [Retirement App](http://10.0.0.5:3838/dmiserak/retirement_tool/)
10. [Radarr Movie Manager](http://10.255.255.1:7878)
11. [Sonarr Show Manager](http://10.255.255.1:8989)
