---
layout: page
title: "Résumé/CV"
menu: "main"
date: 2021-12-01T05:00:00-04:00
draft: false
---

[Curriculum Vitae (PDF)](/pdf/miserak_david-cv.pdf)

## Professional Experience

### Cybersecurity Software Engineer (Contractor)

|||
| --- | --- | --- |
| *Progeny Systems, Information Assurance* | Manassas, VA | **January 2022 - Present** |

#### Company Description

Progeny Systems is a privately held, high-tech small business that
provides systems, services, and products for the Department of
Defense, government agencies, and commercial clients.

#### Role Responsibilities

Responsible for developing new software components to meet program
needs; integration of COTS and GOTS tools, including: anti-virus,
auditing, intrusion detection, application whitelisting, and network
firewalls; unit testing of software components to prepare for
integration in a larger system; conducting software component
integration to ensure end-to-end system functionality; researching,
identifying, and evaluating new technologies for adoption by the US
Navy.

### Software Consultant

|||
| --- | --- | --- |
| *Vermont Data Systems* | Burlington, VT | **September 2014 - Present** |

#### Company Description

Vermont Data Systems is a software consultancy that offers services to
clients looking to improve, create, and/or manage their software
systems.

#### Role Responsibilities

Responsible for collaborating with various stakeholders to determine
software requirements; creating high-level product specifications and
design documents; guiding and assisting the development teams;
troubleshooting and resolving issues with coding or design; and
presenting regular progress reports and setting goals.

#### Achievements

+ Consulted on a 70 million USD project for a benefit system to be
  implemented by 3 state governments. Provided an overview of
  enterprise level best-practices of off-shoring and implemented test
  automation of the happy-path scenario.

+ Designed and programmed a tool for retirement planning and stock
  analysis using end-of-day tick data.

### Sr. Software Engineer (Contractor)

|||
| --- | --- | --- |
| *American Express, Global Encryption Team* | Phoenix, AZ |  **August 2016 - October 2017** |

#### Company Description

The American Express Company is a multi-national financial services
corporation.

#### Role Responsibilities

Responsible for technical leadership of internal and external team
members; contributing to brown-field and green-field projects;
integrating applications into existing systems; determining the root
cause for complex software issues; and working in close partnership
with cross-functional teams and management.

#### Achievements

+ Collaborated with the Hewlett-Packard Enterprise (HPE) Security
  Application Team on file encryption in a resource constrained and
  heterogeneous computing environment for Voltage 5.0 and integration
  with {Ab Initio} systems.

+ Independently identified and engineered a solution (in 6 weeks) for
  a critical project that had been derailed for 18 months by a team of
  11 contractors.

+ Managed a team of 2 to implement a real-time transaction encryption
  system used by over a dozen external clients.

### Software Engineer

|||
| --- | --- | --- |
| *Genomics Lab at UVM* | Burlington, VT | **December 2015 - August 2016** |

#### Company Description

The Human Genetics and Genomics Lab at UVM is a research laboratory
focused on Omics research for Virus-caused cancer, addiction, and
Myalgic Encephalomyelitis/Chronic Fatigue Syndrome (ME/CFS).

#### Role Responsibilities

Responsible for development of databases to compile vast amount of
information (gene expression profiling and protein sequences);
consulting with other scientists and researchers to analyze data sets;
maintaining laboratory documents; attending events and conferences;
and writing grant proposals.

#### Achievements

+ Analyzed, proposed, and programmed web tools with R, Shiny, and
  Bioconductor to display Viral Insertion Polymorphisms (VIPs) with
  other genomic data.

+ Planned, designed, and built a 20 node computing cluster first
  utilizing Apache Hadoop and later Slurm.


### Network Specialist

|||
| --- | --- | --- |
| *Vertek Corp* | Colchester, VT | **November 2014 - November 2015** |

#### Company Description

The Vertek Corporation is a provider of telecommunication, security,
and software services to numerous Managed/Communication Service
Providers (CSPs/MSPs) throughout the United States and Canada.

#### Role Responsibilities

Responsible for working closely with clients and cross-functional
departments to communicate project statuses and proposals; analyzing
data to effectively coordinate the installation of new systems or the
modification of existing systems; monitoring system performance;
developing and executing project plans; administrating computer
networks; and communicating key project data to team members.

#### Achievements

+ Oversaw and coordinated the effort to complete over 1,200 project
  sites.

+ Designed and programmed a software tool to detect faulty systems and
  configure them in bulk that saved a client from losing over $4,000
  in revenue per site a day for over 100 sites. The client later
  signed a 3-year contract.

### Network Operation Center Engineer I

|||
| --- | --- | --- |
| *Sovernet Communications* | Williston, VT | **June 2013 - November 2014** |

#### Company Description

Sovernet is regional provider of high-capacity network transport, broadband Internet, and voice services over its 4500-mile fiber network.

#### Role Responsibilities

Responsible for maintaining and administrating computer networks;
performing disaster recovery operations; protecting data, software,
and hardware from attacks; and working closely with users to identify
potential issues and fix existing problems.

#### Achievements

+ Identified informal company processes and worked with leadership to
  develop documentation for over a dozen acquired heterogeneous
  networks.


## Education
### Vermont Technical College

+ Master of Science in Software Engineering
+ Bachelor of Science in Software Engineering

### Community College of Vermont

+ Associate of Science in Computer Systems Management


### Certifications

|||
| ---   | ---                                         |
| CCENT | Cisco Certified Entry Networking Technician |
| CLA   | C Programming Language Certified Associate  |

## Programming Languages

|||
| ---------------- | ----------------------------------------------------------- |
| Certified        | C |
| Main             | Bash, Core Java SE, Jakarta EE (Java EE, J2EE), Latex, Perl |
| Expericenced     | Ada, JSON, MongoDB, Oracle SQL, R, XML                           |
| Used in the Past | C#, C++, CSS, HTML, JavaScript (ES6), MS SQL, MySQL, PHP, Ruby, x86 Assembly |
| Interests and Curiosities | Haskell, Pascal, PowerShell, Scala, Scheme, Smalltalk, Spark(Ada), TCL/TK |

## Tools and Frameworks

|||
| ---- | -------------------------------------------------------- |
| Main | Git, Hibernate, Maven, NetBeans IDE, Vim, VMWare vSphere |
| Experienced | Citrix XenServer, Eclipse IDE, GoldenGate, IntelliJ IDEA, Nexus, pfSense, Shiny(R), Voltage 4.x |
| Used in the Past | Ab Initio, Docker, EDB Debugger, GlassFish, Gradle, Insight, Jenkins, Jira, JSF, JSP, JUnit, Kubernetes, NASM, Spring, SoapUI, Struts, SVN, Valgrind, Webpack |
| Interests and Curiosities | Ansible, Checkstyle, Concordion, Crucible, Cucumber, Emacs, FindBugs, Fisheye, FitNesse, Frama-C, Hadoop, JBehave, Pega, PMD, Puppet, React, Okta, OpenShift, Semver, SonarQube, Splunk, Travis CI |

## Operating Systems

|||
| --------- | --------------------------------------- |
| Unix      | FreeBSD, IBM AIX                        |
| GNU/Linux | CentOS, Debian, Fedora, Ubuntu          |
| Cisco     | IOS                                     |
