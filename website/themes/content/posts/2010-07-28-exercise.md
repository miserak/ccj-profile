---
title: "Exercise"
date: 2010-07-28T05:00:00-04:00
author: "Harry C. Roff Jr., Lt. Col. Air Force Reserve Retired"
draft: false
---

To: Barbara

From: Harry

1. This pad reminded me of the military style writing.  Answers all
   the questions in the first four lines.
   
2. You have got to walk and walk to get proficient but do it often and
   don't overdue.
   
3. When I was learning to fly I had trouble doing pylon eights.  That
   is, at low level, flying a figure eight holding a wind superimposed
   on a tree then around the next part of the eight on another tree.
   I practiced and practiced until I felt I could now pass an
   inspection but when I stopped I was totally dizzy!
   
4. I will go back to the little note paper where there is not room for
   Air Force stories.
   
5. Love and best wishes.
