---
title: "Stephen's First Step"
date: 2011-05-10T05:00:00-04:00
author: "Harry C. Roff Jr., Lt. Col. Air Force Reserve Retired"
draft: false
---

To: Taffy

From: Dad

1. In the old days when I was a new parent play pens were standard.
This is where a little kid went when dry and fed.

2. A kid learned to roll over, then sit, then stand holding the bars
of the play pen.

3. Steve had achieved the standing, so we all waited to see him let go
   of the bars and stand unaided.
   
4. Whenever Taffy came into the room she would rush over to her little
   brother if he was standing, stick her face into his and shout a
   greeting.
   
5. Steve would let go of the bars and fall like a cut tree.

6. Then he learned - when he saw you, Taffy, he would let go of the
   bars and sit gently.

7. You delayed his first step by two months.

Love,

Dad
