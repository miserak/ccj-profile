---
title: "Repetitive Exercise"
date: 2010-07-29T05:00:00-04:00
author: "Harry C. Roff Jr., Lt. Col. Air Force Reserve Retired"
draft: false
---

To: Barbara

From: Harry

1. The Army Air Corps gave us physical tests for six months after
   recruitment.
   
2. The eye test for convergence consisted of placing a ruler between
   the eyes pointing straight ahead.  Then moving a sliding ball on it
   slowly toward the head.  I was to focus on the ball and follow it
   with both eyes until they eyes had to diverge.
   
3. I couldn't hold the convergence long enough.  I was failing to meet
   the Air Corp requirement.
   
4. The doctor asked me if I really wanted to fly - and my eyes grew
   misty.
   
5. So he said - "once more" and raced the ball in and said I passed.
   He said I should keep practicing by bringing my finder toward my
   eyes. So that was my daily exercise.

6. Best wishes, Harry
