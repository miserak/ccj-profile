---
title: "Undeserved Credit"
date: 2010-07-31T05:00:00-04:00
author: "Harry C. Roff Jr., Lt. Col. Air Force Reserve Retired"
draft: false
---

To: Barbara

From: Harry

1. I think you deserve a lot of credit for facing all your ills so
   bravely.
   
2. When I was an Aviation Cadet I accepted an undeserved credit.  We
   practiced instrument flying under a hood while the instructor sat
   in the rear.  The instructor would put the plane in an unusual
   position and then tell us to recover normal flight.  My instructor
   put the plane in a spin and then said I had the controls.  I
   recognized the spin and then repeated to myself the recovery steps,
   "Pop the stick, kick the rudder." or was it "Kick the rudder, then
   pop the stick." I kept repeating the choices.  The instructor
   called "You've got the controls." I acknowledged and told him I
   recognized the spin. Logic failed me, but the odds were 50/50, so I
   picked one.  Later the instructor told me he admired my monumental
   calm while spinning in to death, but he was never going to fly with
   me again.
