---
title: "Medical Disabilities"
date: 2010-08-04T05:00:00-04:00
author: "Harry C. Roff Jr., Lt. Col. Air Force Reserve Retired"
draft: false
---

To: Barbara

From: Harry

1. They can be a problem or can be used to your advantage.

2. I was in better physical shape than most Aviation Cadets so
   climbing the ten foot obstacle was easy.  Everyone was climbing
   down, I saw the sandpit at the bottom and jumped down.  The sand
   was one inch deep, and I twisted my ankle.
   
3. All underclassmen were forbidden to walk so while everyone else
   ran I walked with my doctor's exemption.
   
4. One day the whole class was late for lunch, so I joined in the run.

5. Suddenly at my side appeared an upperclassman, I handed him my
   doctor's exemption.
