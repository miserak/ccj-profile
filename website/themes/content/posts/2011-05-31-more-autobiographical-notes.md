---
title: "More Autobiographical Notes"
date: 2011-05-31T05:00:00-04:00
author: "Harry C. Roff Jr., Lt. Col. Air Force Reserve Retired"
draft: false
---

To: DJ

From: Grandpa Harry

1. For you DJ,

   The four-page history with dates and a copy of the letter to your
   mother.

2. For your mother; 

   for your mother to add to her books: The May, 10th story of Steve
   and the for page history of dates.

3. Looking forward to seeing you.

Love,

Grandpa Harry
