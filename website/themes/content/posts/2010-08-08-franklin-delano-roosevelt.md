---
title: "Franklin Delano Roosevelt"
date: 2010-08-08T05:00:00-04:00
author: "Harry C. Roff Jr., Lt. Col. Air Force Reserve Retired"
draft: true
---

To: Barbara

From: Harry

1. I was brought up in a Republican family.

2. When I was in high school FDR was driving through my hometown, so
   they let school out, so we could watch.  I went home.  That night my
   father approved of my action.
   
3. When I was a flight instructor in Dover, Delaware FDR died.  Our
   group was to perform a fly-over over the Washington DC funeral of
   FDR.  Only combat pilots.  So all the old P-40 pilots from China
   who filled our administration were resurrected to fly the
   Thunderbolt formation.
   
4. Practice the day before, sixteen planes in V-formations, then the
   old dodo leading put us in echelon to the right, I happened to be
   sixteenth.  Then he said he was going to set course for D.C. and
   turned into the echelon.  So each had to go slower than the plane
   on his left.  I saw the pilot on my left close his throttle.  I
   shut down everything but fell out of formation.
   
6. The day of the funeral I couldn't start my plane, so the spare took
   my place.  I felt my father would be pleased.
