---
title: "New Environment"
date: 2010-08-04T06:00:00-04:00
author: "Harry C. Roff Jr., Lt. Col. Air Force Reserve Retired"
draft: false
---

To: Barbara

From: Harry

1. It will be a big change from hospital to nursing home.  You will
   have to adjust to everything new.
   
2. When the Army Air Corp sent me to Alabama I was unprepared for the
   "Southerner".  Their prejudice to the blacks was the big thing.
   
3. One of my 14 roommates lost a glove, we all looked, but failed.

4. The next day I told him I had found his glove: The "Black" who
   swept the porch was wearing it.
   
5. He was furious.  I asked if he would get it, "Never - he's wearing
   it."
   
6. So I suggested he give the "Black" the other glove.  I was lucky he
   didn't hit me.
