---
title: "Factual Data"
date: 2011-05-30T05:00:00-04:00
author: "Harry C. Roff Jr., Lt. Col. Air Force Reserve Retired"
draft: false
---

To: My Grandson DJ (of whom I am very proud.)

From: Grandpa Harry (Harry C. Roff Jr., Lt. Col. Air Force Reserve Retired)

Date: Memorial Day 2011

1. I enlisted as an Aviation Cadet 1-15-1943 in Hartford, Conn. I was
   ordered to report to Maxwell Field, [Ala.]

2. Eight weeks at Maxwell Field - four as underclassman, four as
   upperclassman.  No planes, drilling, marching, parades, schoolwork,
   physical training.
   
3. Then someone made a choice and I went to Bennettsville S.C. for
   flight training on PT-17's, that was the first time I had ever
   flown.
   
4. Then Sumpter S.C. for basic training on BT-17's. Bi-planes [are]
   primary mono-planes in basic.
   
5. Then Moultree, Ga. for advanced training in AT-6's.

6. Graduated in Nov. 10 1943 as a second lieutenant.

7. Then Clearwater, Florida.  I was pleased that I had been selected
   for fighter aircraft not bombers but the sight of those old P-40's
   like they had been used to fight in China was depressing.  They
   gave us 10 hours of book learning then they got a rush order for
   pilots, so I soloed that day.  Accumulated about 40 hours.
   
8. Then off to the Queen Elizabeth.  Assigned to a private stateroom
   with 14 other guys.  She crossed the North Atlantic in 4½ days.
   
9. Then Grimsby, England up by the wash.  The 78 Fighter Group had
   P-38's - the planes and all their pilots went to Africa leaving the
   Top Group officers and the Top Squadron officers to form a new
   group.  They were assigned P-47's.
   
10. All unassigned pilots were assigned to the new 78th Fighter Group.

11. I was assigned to the 83rd Fighter Squadron.  The other were the
    82nd and the 84th.

12. Our top brass missed their showy P-38's, but we all pushed to get
    hours.  I got about 40 hours in the P-47.
	
13. Then the Group and all three Squadrons were sent to Duxford, Near
    Cambridge, Eng.  The RAF gave us their base, airfield all grass,
    permanent buildings.  The P-47's weighed 7 tons which was harder
    on the grass than the Spitfires.
	
14. We took advantage of the wide grassfield and took off four
    abreast.

15. Our first combat missions were "Fighter Sweeps."  Forty eight
    P-47's at 30,000 feet crossing the channel and flying over
    Hitler's Occupied Territory.
	
16. The Germans could have ignored us, but they felt insulted and flew
    up to challenge.
	
17. Then we started on our key-role which was bomber escort.

18. Bomber crews were assigned a "tour" of 200 hours.  If they
    survived they are rotated home.
	
19. They planned a "tour" for us which would be about 80 missions
    compared to the bomber's 25.
	
20. When my 80 missions were complete I was a first lieutenant with
    79½ missions credited.  Two Enemy shot down, one downed, awarded
    the Air Medal four times which shows as an Air Medal with three
    oak leaf clusters, Distinguished Flying Cross with two clusters,
    and the ETO Theater ribbon with one star.

21. Then I was assigned as a Flight Instructor and I found that duty
    as dangerous as combat.
	
DJ all my flight log books are missing, so this is an overview of my
career.  All done as a lieutenant.  My promotions came in my reserve
duty.  Thanks for sharing.

Grandpa Harry
