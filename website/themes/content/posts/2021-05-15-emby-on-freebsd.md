---
title:  "Emby Media Server on FreeBSD"
date:   2021-05-15 05:00:00-0400
author: "David Miserak"
draft: false
---

```
pkg install emby-server
service emby-server enable
service emby-server start
```
