---
title: "How to get Noticed"
date: 2010-08-07T05:00:00-04:00
author: "Harry C. Roff Jr., Lt. Col. Air Force Reserve Retired"
draft: true
---

To: Barbara

From: Harry

1. There are ways to get noticed in a crowd.  Some are painful, some
   humerus.
   
2. Upper-classmen treated lower-classmen Aviation Cadets poorly.  Se
   we all tried not to attract attention to ourselves.
   
3. One day I spotted an upper-classmen with whom I enlisted.  In all
   honesty I said to him: "You aren't as big a bastard as the rest of
   the upper-classmen."
   
4. At the end of each day all upper and lower form up in the parade
   ground to salute the flag and end the day.

5. Oh, something special.  I was called front and center, asked it I
   would repeat my remarks to Saperstein.
   
6. I said I forgot, so the Commander quoted it and asked if that were
   correct.
   
7. Lying is out so I admitted it.

8. The laughter from hundreds convinced me that this was a big joke.

9. I was sentenced to run three times around the parade ground with my
   arms out and making a noise like an airplane.
   
10. I hope you can find something mildly amusing in all the shuffling
    around with you at then center.  If well wishes will help you have
    all mine.
	
    Harry
